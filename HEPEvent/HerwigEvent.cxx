/*
   HerwigEvent class implementation

   AUTHOR:      Piotr Golonka 
   LAST UPDATE: 2003-04-28
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "HerwigEvent.H"

#ifdef _USE_ROOT_
ClassImp(HerwigEvent)
#endif


HerwigEvent HerwigEVT; 

extern "C" HepevtCommon hepevt_;

//_____________________________________________________________________________
HerwigEvent::HerwigEvent(int size):
hparticles(new HerwigParticle[size])
{
    _size=size;
    data=&hepevt_;

   for (int i=1; i<=_size;i++) {
     hparticles[i-1].SetId(i);
    }
}

//_____________________________________________________________________________
HerwigEvent::HerwigEvent(void* dataptr, int size):
hparticles(new HerwigParticle[size])
{
    _size=size;
    data=dataptr;

  for (int i=1; i<=_size;i++) {
    hparticles[i-1].SetId(i);
    hparticles[i-1].SetEvent(this);
  }

}

//_____________________________________________________________________________
void HerwigEvent::ls( char *option)
{
   printf("\nHerwigEvent number %i\n",GetNEVHEP());

//    for (int i=1; i<=GetNumOfParticles(); i++) {
//        HEPParticle *p=GetParticle(i);
//	if (p) p->ls(option);
//
//    }
   HEPEVTEvent::ls(option);
   printf("\n");
}  

#ifdef _USE_ROOT_
void HerwigEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility - dummy
}
#endif  
