/*
   MC4Vector implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 2003-10-26
*/

#include "MC4Vector.H"
#include "MC3Vector.H"

#ifdef _USE_ROOT_
ClassImp(MC4Vector)
#endif
#include <math.h>  //sqrt()

const double _PI_=3.14159265358979323846264338327;


//______________________________________________________________________
MC4Vector::MC4Vector():
v_X0(0.0),
v_X1(0.0),
v_X2(0.0), 
v_X3(0.0), 
v_M(0.0)
{
}


//______________________________________________________________________
MC4Vector::MC4Vector(double x0, double x1, double x2, double x3):
  v_X0(x0),
  v_X1(x1),
  v_X2(x2),
  v_X3(x3),
  v_M(0)
{
  // Constructor   - called when object is created.
  // 
  // Sets components of 4-vector to specified values,
}

//______________________________________________________________________
MC4Vector::MC4Vector(double x0, double x1, double x2, double x3, double m):
  v_X0(x0),
  v_X1(x1),
  v_X2(x2),
  v_X3(x3),
  v_M (m)
{
  // Constructor   - called when object is created.
  // 
  // Sets components of 4-vector and its additional m field 
  // to specified values

}

//______________________________________________________________________
MC4Vector::MC4Vector( MC4Vector const &v):
     v_X0 ( v.v_X0 ),
     v_X1 ( v.v_X1 ),
     v_X2 ( v.v_X2 ),
     v_X3 ( v.v_X3 ),
     v_M  ( v.v_M  )
{
  // Copy constructor - called when a copy of an object is being created.
}

//______________________________________________________________________
MC4Vector::MC4Vector( MC4Vector const *v):
     v_X0 ( v->v_X0 ),
     v_X1 ( v->v_X1 ),
     v_X2 ( v->v_X2 ),
     v_X3 ( v->v_X3 ),
     v_M  ( v->v_M  )
{
  // Copy constructor which accepts pointer to original object
}

//______________________________________________________________________
void MC4Vector::Get( double &x0, double &x1, double &x2, double &x3)
{
  // Returns component of 4vectors in arguments.
  
  x0 = v_X0;
  x1 = v_X1;
  x2 = v_X2;
  x3 = v_X3;

}


//______________________________________________________________________
void MC4Vector::Set(MC4Vector *v)
{
  // Sets own coordinates' values to the ones specified in argument.

  v_X0 = v->v_X0;
  v_X1 = v->v_X1;
  v_X2 = v->v_X2;
  v_X3 = v->v_X3;
  v_M  = v->v_M;
}


//______________________________________________________________________
void MC4Vector::Set( double x0, double x1, double x2, double x3, double m)
{
  // Sets components of 4 vectors to these values.
  //
  // m parameter is optional - if m is not specified( or value -1e30 is passed)
  // m field is not set.
  //

  v_X0 = x0;
  v_X1 = x1;
  v_X2 = x2;
  v_X3 = x3;

  if (m != -1e30)
    v_M = m;

}


//______________________________________________________________________
MC4Vector MC4Vector::operator+( const MC4Vector v )
{
  // Addition operator.
  // 
  // Returns sum of this and v ;
  // Example:
  //
  //         MC4Vector v1(0 ,1 ,2 ,3 );
  //         MC4Vector v2(10,11,12,13);
  //
  //         MC4Vector sum = v1 + v2;
  //
  // sum will be MC4Vector( 10,12,14,16).

  return MC4Vector( v_X0 + v.v_X0 ,
 		    v_X1 + v.v_X1 ,
		    v_X2 + v.v_X2 ,
		    v_X3 + v.v_X3 );
}

//______________________________________________________________________
MC4Vector MC4Vector::operator-( const MC4Vector v )
{
  // Subtraction operator.
  // 
  // Returns difference between this and v
  // Example:
  //
  //         MC4Vector v1(10,11,12,13);
  //         MC4Vector v2(0 ,1 ,2 ,3 );
  //
  //         MC4Vector dif = v1 - v2;
  //
  // dif will be (0,10,10,10).

  return MC4Vector( v_X0 - v.v_X0 ,
		    v_X1 - v.v_X1 ,
		    v_X2 - v.v_X2 ,
		    v_X3 - v.v_X3 );
}

//______________________________________________________________________
MC4Vector MC4Vector::operator-()
{
  // Unary minus operator.
  //
  // returns a vector with oposite coords.
  // Example:
  //
  //         MC4Vector v( 5, -3, -2, 4);
  //
  //         MC4Vector w = -v;
  //
  // w will be MC4Vector(-5,3,2,-4)

  return MC4Vector( - v_X0,
		    - v_X1,
		    - v_X2,
		    - v_X3 );
}

//______________________________________________________________________
MC4Vector MC4Vector::operator+()
{
  // Unary plus operator.
  //
  // returns a vector with same coords - this is complementary
  // operator to MC4Vector::operator-()
  //
  // Example:
  //
  //         MC4Vector v( 5, -3, -2, 4);
  //
  //         MC4Vector w = +v;
  //
  // w will be MC4Vector(5,-3,-2,4)
  return MC4Vector( v_X0,
		    v_X1,
		    v_X2,
		    v_X3 );
}

//______________________________________________________________________
MC4Vector& MC4Vector::operator+=( const MC4Vector v )
{
  // Self addition operator.
  // 
  // Adds v to this:
  //
  // Example:
  //
  //         MC4Vector v1(1, 2, 3, 4);
  //         MC4Vector v2(5, 6, 7, 8);
  //
  //         v1 += v2;
  //
  // v1 will be (6,8,10,12)
 
  v_X0 += v.v_X0;
  v_X1 += v.v_X1;
  v_X2 += v.v_X2;
  v_X3 += v.v_X3;
  return *this;
}

//______________________________________________________________________
MC4Vector& MC4Vector::operator-=( const MC4Vector v )
{
  // Self subtraction operator.
  // 
  // subtracts v from this:
  //
  // Example:
  //
  //         MC4Vector v1(1, 2, 3, 4);
  //         MC4Vector v2(5, 0, 2, 20);
  //
  //         v1 -= v2;
  //
  // v1 will be (-4,2,1,-16)
 
  v_X0 -= v.v_X0;
  v_X1 -= v.v_X1;
  v_X2 -= v.v_X2;
  v_X3 -= v.v_X3;
  return *this;
}

//______________________________________________________________________
double MC4Vector::operator*( MC4Vector v )
{
  // Scalar product operator.
  //
  // Returns scalar product according to metric tensor g=(1,-1,-1,-1) .
  //
  //              k      0 0   ->   ->
  // B * A = B   A  =   B A  - B  * A
  //          k
  //
  //
  // Example:
  //         MC4Vector v1=(0,1,0,0);
  //         MC4Vector v2=(0,0,1,0);
  //
  //         double p = v1*v2;
  //
  // p will be 0, because vectors are orthogonal in this example.
  //

  return (   v_X0 * v.v_X0 
	   - v_X1 * v.v_X1 
	   - v_X2 * v.v_X2 
	   - v_X3 * v.v_X3 );
}

//______________________________________________________________________
MC4Vector& MC4Vector::operator=( const MC4Vector &v)
{
  // Assignment operator - called whenever one vector is assigned to another
  // 
  // Assigns x0 - x3 components.
  //
  // *** WARNING ! Does not assign m field !
  //               To assign a vector to another with m field one rather use
  //               Set() operator.
  //
  // Example:
  //
  //         MC4Vector u(1,2,3,4,5);
  //         MC4Vector v(0,0,0,0,0);
  //         MC4Vector w(0,0,0,0,0);
  //
  //         v = u;
  //         w.Set(u)
  //
  // v will be (1,2,3,4,[0]), while w will be (1,2,3,4,[5]) 
  //  ( [] denotes m field value)
  //

   v_X0 = v.v_X0;
   v_X1 = v.v_X1;
   v_X2 = v.v_X2;
   v_X3 = v.v_X3;

  
  return *this;
}

//______________________________________________________________________
const bool MC4Vector::operator==( const MC4Vector &v)
{
  // Equality operator.
  //
  // return true(1) when 4-vectors are same.
  //
  // Note: does not compare additional m field.
  //

  if( v_X0 == v.v_X0 &&
      v_X1 == v.v_X1 &&
      v_X2 == v.v_X2 &&
      v_X3 == v.v_X3    ) 
    return 1;
  else
    return 0;
}



//______________________________________________________________________
double MC4Vector::Phi()
{
  //Phi angle - anti-clockwise.

  return Angle1(GetX1(),GetX2());
}

//______________________________________________________________________
double MC4Vector::Theta()
{
  //Theta angle - anti-clockwise.

  double r = sqrt( GetX1() * GetX1() + 
		   GetX2() * GetX2() );
  return Angle2(GetX3(),r);
} 

//______________________________________________________________________
double MC4Vector::Angle1(double x, double y)
{
  // Angle1() - utility function used by Phi().
  //
  // Returns phi angle (anti-clockwise) of
  // 2D point of coordinates (x,y).

  double angle=Angle2(x,y);

  if (y < 0.0)
    angle = 2.0 * _PI_ - angle;

  return angle;
}	 

//______________________________________________________________________
double MC4Vector::Angle2(double z, double r)
{
  // Angle2() - utility function used by Theta().
  //

  double angle=0.0;
  
  if ( fabs(r) > fabs(z) ) {
    angle = atan( fabs(r/z) ) ;
    
    if (z <= 0.0) angle = _PI_ - angle;

  } else if ( (z != 0.0) && (r != 0.0) ) {
    angle = acos ( z / sqrt(z*z+r*r) );

  }

  return angle;

}	 

//______________________________________________________________________
double MC4Vector::Length()
{
  // Length of (X1,X2,X3) vector:
  //     ________________
  //   \/ x*x + y*y + z*z
  //
   return sqrt( Length2() );

}   

//______________________________________________________________________
double MC4Vector::Length2()
{
  // Sqare of Length of (X1,X2,X3) vector:
  //
  //  2    2    2
  // x  + y  + z
 
   return ( GetX1()*GetX1() + GetX2()*GetX2() + GetX3()*GetX3() );

}   

//______________________________________________________________________
double MC4Vector::Xt()
{
  //Returns transverse component of the vector (e.g. p_t),
  // longitudal axis is along z(X3).
  //         _______
  // X_t = \/x*x+y*y

  double xt = sqrt( GetX1() * GetX1() + 
		   GetX2() * GetX2() );
  return xt;
} 


//______________________________________________________________________
double MC4Vector::Square()
{
  // Sqare of 4-vector, according to scalar tensor g=(1,-1,-1,-1) 
  // See also MC4Vector::operator*()
  // i.e: 
  //       2     2     2
  //      M  =  E  -  p
  //
  
   return ( GetX0()*GetX0() - 
            GetX1()*GetX1() - 
	    GetX2()*GetX2() - 
	    GetX3()*GetX3() );

}   

//______________________________________________________________________
void MC4Vector::AdjustM()
{
  // Sets m field to
  //     _____________________
  //    /  2     2     2     2
  //  \/ X   - X   - X   - X
  //      0     1     2     3
  //
  // This function is designated for cases with MC4Vector objects
  // representing energy-momentum vectors.
  // In this case AdjustM will calculate mass value according
  // to invariant expression:
  //          2    2    2
  //         E  - p  = m
  //
  // if expression under sqare root is negative, warning is printed
  // and m field remains untouched.
  

  double sqr = Square();
  
  if ( sqr >= 0 )
    v_M  = sqrt( Square() );
  else
    printf("WARNING! MC4Vector::AdjustM() - E^2 - p^2 is negative! Calculation abandoned.\n");
}



//______________________________________________________________________
void MC4Vector::Boost(double gamma, double beta1, double beta2, double beta3)
{
  // Boost parametrized by gamma and beta parameters.

    double x0_ ;
    double x1_ ;
    double x2_ ;
    double x3_ ;
    double BX  ;
    double d   ;
    
    if (!beta1 & !beta2 & !beta3) return;
    
    // calculate new values:
    
    //scalar product beta.X :
    BX  =  beta1 * GetX1()  +  beta2 * GetX2()  +  beta3 * GetX3();

    //temporary value d=(beta.X)*(gamma-1)/(beta^2)
    d=BX*(gamma-1)/(beta1*beta1+beta2*beta2+beta3*beta3);

    x0_ =  gamma * ( GetX0() - BX );

    x1_ =  GetX1() - gamma*beta1*GetX0() +  beta1*d;
    x2_ =  GetX2() - gamma*beta2*GetX0() +  beta2*d;
    x3_ =  GetX3() - gamma*beta3*GetX0() +  beta3*d;
    
    // Set these values as new ones:
    SetX0(x0_);
    SetX1(x1_);
    SetX2(x2_);
    SetX3(x3_);
    
}

//______________________________________________________________________
void MC4Vector::Boost(double p1, double p2, double p3,double E, double m)
{
  // Boost parametrized by particle's momentum, Energy and Mass.
  double betx=-p1/m;
  double bety=-p2/m;
  double betz=-p3/m;

  double gam=E/m;

  double pb=betx*GetX1()+bety*GetX2()+betz*GetX3();

  SetX1(GetX1()+betx*(GetX0()+pb/(gam+1.0)));
  SetX2(GetX2()+bety*(GetX0()+pb/(gam+1.0)));
  SetX3(GetX3()+betz*(GetX0()+pb/(gam+1.0)));

  SetX0(GetX0()*gam+pb);            

}

//______________________________________________________________________
void MC4Vector::Boost(double beta1, double beta2, double beta3)
{
  // Boost parametrized by beta only - gamma calculated from beta.

  // Firstly: calculate gamma
  double beta_square=(beta1*beta1+beta2*beta2+beta3*beta3);
  if (beta_square>1.0)
    {
      printf("ERROR in MC4Vector::Boost(beta1, beta2, beta3)! \n");
      printf("Absolute value of specified beta = %f exceeds 1.0 \n\n",
	     sqrt(beta_square));
      return;
	}
  double gamma = 1.0/sqrt(1.0 - beta_square);
  printf("gamma = %f ; |beta|= %f \n",gamma,sqrt(beta_square));    
  // and now boost it!
  Boost(gamma, beta1, beta2, beta3);     


}
//______________________________________________________________________
void   MC4Vector::Boost3(double expEta)
{
  // Boost in Z direction parametrized by exp(eta) parameter.
  //
  // algorithm taken from photos module by Zbigniew Was.
  //

  double qpl = (GetX0() + GetX3())*expEta;
  double qmi = (GetX0() - GetX3())/expEta;

  SetX3( (qpl-qmi)/2.0 );
  SetX0( (qpl+qmi)/2.0 );

}


//______________________________________________________________________
void MC4Vector::Rotate1(double angle)
{
  // Rotate along x (1st) axis anti-clock-wise (!)
  //
  //      z                  _   _     __             __     _   _
  //      |                 |     |   |                 |   |     |
  //      |                 |  y' |   | cos a   - sin a |   |  y  |
  //      |                 |     | = |                 | * |     |
  //      |                 |  z' |   | sin a     cos a |   |  z  |
  //      |__________y      |_   _|   |__             __|   |_   _|
  //     /                  
  //    x
  //

  double yy =  cos(angle)*GetX2() - sin(angle)*GetX3();
  double zz =  sin(angle)*GetX2() + cos(angle)*GetX3();

  SetX2(yy);
  SetX3(zz);

}

//______________________________________________________________________
void MC4Vector::Rotate2(double angle)
{
  // Rotate along y (2nd) axis anti-clockwise (!)
  //
  //      x                  _   _     __             __     _   _
  //      |                 |     |   |                 |   |     |
  //      |                 |  z' |   | cos a   - sin a |   |  z  |
  //      |                 |     | = |                 | * |     |
  //      |                 |  x' |   | sin a     cos a |   |  x  |
  //      |__________z      |_   _|   |__             __|   |_   _|
  //     /                  
  //    y
  //

  
  double zz =  cos(angle)*GetX3() - sin(angle)*GetX1();
  double xx =  sin(angle)*GetX3() + cos(angle)*GetX1();

  SetX1(xx);
  SetX3(zz);
    
}

//______________________________________________________________________
void MC4Vector::Rotate3(double angle)
{
  // Rotate along z (3rd) axis anti-clockwise (!);
  //
  //      y                  _   _     __             __     _   _
  //      |                 |     |   |                 |   |     |
  //      |                 |  x' |   | cos a   - sin a |   |  x  |
  //      |                 |     | = |                 | * |     |
  //      |                 |  y' |   | sin a     cos a |   |  y  |
  //      |__________x      |_   _|   |__             __|   |_   _|
  //     /                  
  //    z
  //


  double xx =  cos(angle)*GetX1() - sin(angle)*GetX2();
  double yy =  sin(angle)*GetX1() + cos(angle)*GetX2();


  SetX1(xx);
  SetX2(yy);

}




//______________________________________________________________________
void MC4Vector::ls(char *option)
{
  // ls - prints 4-vector.
  //
  // *option parameter is dummy.
  //
  if (option);  
  
  printf("(% .12g , % .12g , % .12g , % .12g) [% .12g]\n",
         v_X0,v_X1,v_X2,v_X3, v_M);    
}


MC3Vector MC4Vector::Get3Vector()
{
    return MC3Vector(v_X1, v_X2, v_X3);
}


MC4Vector::~MC4Vector()
{
}


double  MC4Vector::GetX0()            { return v_X0;            }
double  MC4Vector::GetX1()            { return v_X1;            }
double  MC4Vector::GetX2()            { return v_X2;            }
double  MC4Vector::GetX3()            { return v_X3;            }
double  MC4Vector::GetM ()            { return v_M;             }



void MC4Vector::Boost(MC4Vector &v)
{
// here: boost parameters specified as (gamma, betaX,betaY,betaZ)
    Boost(v.GetX0(), v.GetX1(), v.GetX2(), v.GetX3());
}

void MC4Vector::BoostP(MC4Vector &p)
{
// here: boost parameters specified as if it were particle's momentum,
// i.e. gamma=E/m , beta=P_vec/m
// WARNING! M needs to be set properly (e.g. using v.AdjustM()).
/*
    double gamma=p.GetX0()/p.GetM();
    double betaX=p.GetX1()/p.GetM();
    double betaY=p.GetX2()/p.GetM();
    double betaZ=p.GetX3()/p.GetM();
*/
//printf("BoostP: gamma=%f beta=%f %f %f\n",gamma,betaX,betaY,betaZ);

//    Boost(gamma, betaX, betaY, betaZ);
    Boost(p.GetX1(), p.GetX2(), p.GetX3(), p.GetX0(),p.GetM());
}

