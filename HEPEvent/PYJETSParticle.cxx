/*
   PYJETSParticle class implementation

   AUTHOR:      Piotr Golonka 
   LAST UPDATE: 2000-01-24
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "PYJETSParticle.H"
#include "PYJETSEvent.H"

#ifdef _USE_ROOT_
ClassImp(PYJETSParticle)
#endif


PYJETSParticle::PYJETSParticle()
{
 id=1;
 event=&PYJETS;
}

void PYJETSParticle::ls (char* option )
{
   if (option==0) {
      printf("%3i%7s [%2i] <%3i|%3i,%3i> (% 8.3f,% 8.3f,% 8.3f)%8.3f %8.3f\n",
            GetId(), GetParticleName(),GetStatus(),
            GetMother(), GetFirstDaughter(), GetLastDaughter(),
            GetPx(), GetPy(), GetPz(), GetE(), GetM()
            );
      }
}


inline HEPEvent* PYJETSParticle::GetEvent()
{ 
  return (HEPEvent*) event; 
}

inline int const PYJETSParticle::GetId()
{ 
   return id;
 }

inline int const PYJETSParticle::GetMother()
{ 
   return event->GetK_(id,3);
}

inline int const PYJETSParticle::GetMother2()
{ 
   return 0;
}

inline int const PYJETSParticle::GetFirstDaughter()
{ 
   return event->GetK_(id,4);
}

inline int const PYJETSParticle::GetLastDaughter()
{ 
   return event->GetK_(id,5);
}

inline double const PYJETSParticle::GetE ()
{ 
   return event->GetP_(id,4);
}

inline double const PYJETSParticle::GetPx()
{ 
   return event->GetP_(id,1);
}

inline double const PYJETSParticle::GetPy()
{
   return event->GetP_(id,2);
}

inline double const PYJETSParticle::GetPz()
{ 
return event->GetP_(id,3);
}

inline double const PYJETSParticle::GetM()
{
   return event->GetP_(id,5);
}

inline int const PYJETSParticle::GetPDGId()
{ 
   return event->GetK_(id,2);
}

inline int const PYJETSParticle::GetStatus()
{
   return event->GetK_(id,1);
}

inline double const PYJETSParticle::GetVx()
{ 
   return event->GetV_(id,1);
}

inline double const PYJETSParticle::GetVy()
{
   return event->GetV_(id,2);
}

inline double const PYJETSParticle::GetVz()
{
   return event->GetV_(id,3);
}

inline double const PYJETSParticle::GetTau()
{ 
   return event->GetV_(id,4);
}

inline double const PYJETSParticle::GetLifetime()
{ 
   return event->GetV_(id,5);
}

inline void PYJETSParticle::SetEvent( HEPEvent  *event ) 
{
    this->event=(PYJETSEvent*)event;
//  printf("Unsupported : PYJETSParticle::SetEvent\n");
}

inline void PYJETSParticle::SetId( int id       )
{
   this->id=id;
}     

inline void PYJETSParticle::SetMother( int mother )
{ 
  event->SetK_(id,3,mother);
}

inline void PYJETSParticle::SetMother2( int mother ) 
{

}

inline void PYJETSParticle::SetFirstDaughter( int daughter )
{
   event->SetK_(id,4,daughter);
}

inline void PYJETSParticle::SetLastDaughter( int daughter )
{
    event->SetK_(id,5,daughter); 
}

inline void PYJETSParticle::SetE( double E )
{
   event->SetP_(id,4,E ); 
}

inline void PYJETSParticle::SetPx( double px )
{
   event->SetP_(id,1,px); 
}

inline void PYJETSParticle::SetPy( double py )
{ 
   event->SetP_(id,2,py); 
}

inline void PYJETSParticle::SetPz( double pz )
{
   event->SetP_(id,3,pz); 
} 

inline void PYJETSParticle::SetM( double m )
{
   event->SetP_(id,5,m ); 
}

inline void PYJETSParticle::SetPDGId( int pdg )
{
   event->SetK_(id,2,pdg); 
}

inline void PYJETSParticle::SetStatus( int st )
{ 
   event->SetK_(id,1,st); 
}

inline void PYJETSParticle::SetVx( double vx )
{
   event->SetV_(id,1,vx); 
}

inline void PYJETSParticle::SetVy( double vy )
{
   event->SetV_(id,2,vy); 
}

inline void PYJETSParticle::SetVz( double vz )
{
   event->SetV_(id,3,vz); 
}

inline void PYJETSParticle::SetTau( double tau )
{
   event->SetV_(id,4,tau); 
}

inline void PYJETSParticle::SetLifetime(double lifetime )
{
   event->SetV_(id,5,lifetime); 
}






#ifdef _USE_ROOT_
void PYJETSParticle::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}
#endif   
