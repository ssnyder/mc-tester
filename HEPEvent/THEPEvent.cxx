/*
   THEPEvent class implementation

   AUTHOR:      Piotr Golonka 
   LAST UPDATE: 1999-04-21
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "THEPEvent.H"
#include "THEPParticle.H"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#ifdef _USE_ROOT_
ClassImp(THEPEvent)
#endif


// global pointer for general-purpose THEPEvent object
THEPEvent *gHEPEvent = 0;




//_____________________________________________________________________________
void  THEPEvent::InsertParticle(int at_idx,HEPParticle *p)
{
   printf("UNSUPPORTED method THEPEvent::AddParticle()\n");

}

//_____________________________________________________________________________
void  THEPEvent::AddParticle( int id, int pdgid, int status,
			      int mother, int mother2,
			      int firstdaughter, int lastdaughter,
			      double E,double px, double py, double pz, double m,
			      double vx, double vy, double vz, double tau)
{
   printf("UNSUPPORTED method THEPEvent::AddParticle()\n");

}




THEPEvent::THEPEvent():
  fParticles(0)
{
    // default constructor - WARNING! It mustn't be used to instantiate
    // objects - only as a reference for derived classes, because it does not
    // initialize a storage space for THEPParticle objects. That's why it is 
    // made protected. To crete object use THEPEvent::THEPEvent(int num) .
    
}

THEPEvent::THEPEvent(int num):
  fParticles((HEPParticle**)new THEPParticle*[num+1]),
  fSize(num)
{
  // constructor. Creates new THEPEvent object.

  fEventNumber    = 0;
  fNumOfParticles = 0;
  fParticles[0]   = nullHEPParticle;

  // clear the array - we'll be sure that we won't 
  // point out to blue sky...
  for (int i=1;i<num;i++)
    fParticles[i]=0;

}



THEPEvent::THEPEvent(const THEPEvent &e):
  fParticles((HEPParticle**)new THEPParticle*[e.fSize]),
  fSize(e.fSize)
{
  // copying constructor - have to make a copy of all particles:

  fNumOfParticles = e.fNumOfParticles;
  fEventNumber    = e.fEventNumber;
  fParticles[0] = nullHEPParticle;

  for (int i=1 ; i<=fNumOfParticles ; i++) {
  
    THEPParticle *p = ((THEPParticle**)(e.fParticles))[i];
    fParticles[i]   = new THEPParticle(*p);
  
  }

}


THEPEvent::~THEPEvent()
{
  // destructor. Deletes all managed THEPParticle objects .
  Clear();
  
  
  // delete array too!
  delete fParticles;   
}



const THEPEvent& THEPEvent::operator=(const THEPEvent &e)
{
  // assignment operator. Replaces the current event
  // with a given one by creating a copy of all particles(!)

  if (&e != this) {
    Clear();
    fNumOfParticles = e.fNumOfParticles;
    fEventNumber    = e.fEventNumber; 
    
    for (int i=1 ; i <= fNumOfParticles ; i++)
      {	
	THEPParticle *p = ((THEPParticle**)(e.fParticles))[i];
	fParticles[i]   = new THEPParticle(*p);
      }
  }
  return *this;
}



bool THEPEvent::isEqual(const THEPEvent &e)
{
  // comparison cunction: returns 1 if event sizes
  // of this and given THEPEvent object matches, and
  // all particles in both events are the same.
  // Otherwise returns 0.

  bool result = 1;
  if (fNumOfParticles != e.fNumOfParticles)
    result = 0;
  
  for (int i=1; i<=fNumOfParticles; i++) {
    THEPParticle *p  = ((THEPParticle**)fParticles)[i];
    THEPParticle *ep = ((THEPParticle**)(e.fParticles))[i];
    
    if ( (*p) != (*ep) ) {
//      printf("Difference in particle %i\n",i);
      result = 0;
      break;
    }
  }
return result;
}

void THEPEvent::Diff(const HEPEvent &ev)
{
  // prints a list of differences between this event and another THEPEvent
  // object.
  THEPEvent &e= (*((THEPEvent*)(&ev) ));
  char full[] ="full";

  if ( (*this) != (THEPEvent)e) {

    int maxnum = fNumOfParticles;
    int minnum = e.fNumOfParticles;
    int bigger = 1;
    if (e.fNumOfParticles>maxnum) {
      maxnum = e.fNumOfParticles;
      minnum = fNumOfParticles;
      bigger = 0;
    }
    
    int ptr  = 0;
    int eptr = 0;
    //    printf("Events' differences:\n");
    if (fNumOfParticles != e.fNumOfParticles)
      printf("NumOfParticles: %i | %i\n",fNumOfParticles,e.fNumOfParticles);

    while (ptr< fNumOfParticles && eptr<e.fNumOfParticles) {
      ptr++;
      eptr++;
      HEPParticle *p  = ((HEPParticle**)fParticles)[ptr];
      HEPParticle *ep = ((HEPParticle**)(e.fParticles))[eptr];
      if ( p->Compare_WithoutId(*ep) )
	continue;
      

      // if different we have to check, whether it's not just an additional p.
      
      // we firstly look through (*this) ti find something that matches (*ep)
      if (bigger) {
	int k=ptr;
	int found_at=0;
	for (int l=k; l<=fNumOfParticles;l++) {
	  if (( ((HEPParticle**)fParticles)[l] )->Compare_WithoutId(*ep)) {
	    found_at=l;
	    break;
	  }
	}
	// we must print out the particles that are between:
	if (found_at) {
	  for (int ll=k;ll<found_at;ll++){
	    printf("######################################################################\n");
	    printf("new particle:\n");
	    fParticles[ll]->ls(full);
	    printf("######################################################################\n");
	  }
	  ptr=found_at;
	} else {
	  // look through the other to find new ones:
	  for (int lll=eptr;lll<=e.fNumOfParticles;lll++){
	    if (e.fParticles[lll]->GetPDGId()==p->GetPDGId()){
	      (e.fParticles[lll])->Diff(*p);
	      eptr=lll;
	      break ;
	    }
	    printf("######################################################################\n");
	    printf("new particle:\n");
	    e.fParticles[lll]->ls();
	    printf("######################################################################\n");
	  }
	}
      } else { // if not bigger -do it in oposite direction

	int k=eptr;
	int found_at=0;
	for (int l=k; l<=e.fNumOfParticles;l++) {
	  if (e.fParticles[l]->Compare_WithoutId(*p)) {
	    found_at=l;
	    break;
	  }
	}
	// we must print out the particles that are between:
	if (found_at) {
	  for (int ll=k;ll<found_at;ll++){
	    printf("######################################################################\n");
	    printf("new particle:\n");
	    e.fParticles[ll]->ls(full);
	    printf("######################################################################\n");
	  }
	  eptr=found_at;
	} else {
	  // look through the other to find new ones:
	  for (int lll=ptr;lll<=fNumOfParticles;lll++){
	    if (fParticles[lll]->GetPDGId()==ep->GetPDGId()){
	      (fParticles[lll])->Diff(*ep);
	      ptr=lll;
	      break;
	    }
	    printf("######################################################################\n");
	    printf("new particle:\n");
	    fParticles[lll]->ls();
	    printf("######################################################################\n");
	  }
	}
      }
    }
  }
}



void THEPEvent::AddParticle( HEPParticle *p)
{
  // Adds particle at the end of a record.
//  printf("Adding particle at %i:\n",fNumOfParticles+1);
  //p->ls("full");

  if (fNumOfParticles>=fSize)
    {
      printf("ERROR in THEPEvent:AddParticle!\n");
      printf("No space to add a new particle - particle not added!\n");
      return;
    }
  
  fParticles[++fNumOfParticles]=p;
  
}


HEPParticle* THEPEvent::GetParticle(int idx)
{
  // returns particle at position (idx).
  // if there's no particle at requested position, returns
  // nullHEPParticle object.

  THEPParticle *particle = nullHEPParticle;

  if (idx<=fNumOfParticles && idx>0 ){
    if ( fParticles[idx] )
      particle = ((THEPParticle**)fParticles)[idx];
  }else {
    printf("WARNIG in THEPEvent:GetParticle! idx=%i is out of range [1,%i]!\n",
	   idx,fNumOfParticles);
  }      
  return particle;
}

void THEPEvent::Clear(int fromIdx)
{
  // Clears event record from position (fromIdx). If no argument
  // is specified, 1 is assumed that means clearing all particles from
  // event.
  // All cleared THEPParticle objects are DELETED!!!


  if (fromIdx >= fNumOfParticles+1)
    return;
  if (fromIdx < 1 || fromIdx >= fNumOfParticles+1) {
    printf("Error in Clear() fromIdx=%i is out of range[1,%i] .\n",
	   fromIdx,fNumOfParticles);
    return;
  }

for (int i=fromIdx;i<=fNumOfParticles;i++)
  if (fParticles[i] && fParticles[i]->GetPDGId()!=0){
    delete (fParticles[i]);
    fParticles[i]=0;
  }
 fNumOfParticles=fromIdx-1;
}



void THEPEvent::SetParticle(int idx,HEPParticle *p)
{
  // Sets a particle at a specified position.
  // WARNING! User is responsible to dispose the object that previously
  // was present on this position!!! no delete is done!
  // (look at commented line in a source).
  if (idx <1 || idx >fNumOfParticles)
    {
      printf("ERROR in THEPEvent::SetParticle(int idx,THEPParticle *p)\n");
      printf(" You want to set a particle at idx=%i which is out of range\n",
	     idx);
      printf("[1..%i] Operation abandoned.\n",
	     fNumOfParticles);
      return;
    }
  
  if (!p)
    {
      printf("ERROR in THEPEvent::SetParticle(int idx,THEPParticle *p)\n");
      printf(" You want to add a null-pointer as a particle!\n"); 
      printf("Operation abandoned.\n");
      return;
    }
  
  //  if (fParticles[idx]) delete fParticles[idx];
  fParticles[idx]=(THEPParticle*)p;
}        

void THEPEvent::ls(char *option)
{
  // makes a printout of an event depending on (*option):
  // if option=0 (default) - short listing
  // (*option)="P"         - particles' momenta are listed with high
  //                         precision.
  // otherwise             - dump of all particles' data.
  // 
  // See also: THEPParticle::ls

  double pxsum=0;
  double pysum=0;
  double pzsum=0;
  double m=0;
  double esum=0;
  THEPParticle *p=0;

  printf("\nTHEPEvent: event number %i , number of particles: %i\n",
	 fEventNumber,fNumOfParticles);
  printf("===============================================================================\n");

  if(option) {

    if(strstr(option,"P")) {
      printf("ID. Particle <MOTHER ; CHILD >(      Px       ,          Py   ,          Pz    )     E                M      \n");
      printf("---|--------|<-------;------->(---------------,---------------,----------------)---------------|---------------\n");
      
    } else {
      printf("ID. Particle (PDG) <MOTHER ; CHILD > STATUS (   Px    ,    Py   ,   Pz    )     E        M      (   Vx  ,   Vy  ,   Vz  )  tau\n");
      printf("---|--------|-----|--------|--------|------|----------|---------|----------|---------|---------|--------|-------|--------|---------\n");
    }
  } else {
    printf("ID. Particle < Origin> STAT (   Px    ,    Py   ,   Pz    )     E        M \n");
    printf("---|--------|-----|---|----|----------|---------|----------|---------|---------\n");

  }
  for (int i=1;i<=fNumOfParticles;i++){
    p=((THEPParticle**)fParticles)[i];
   
    if (p){ //this is "=" not "=="!!!
      
      p->ls(option);
      
      if (p->GetStatus()==1){
	pxsum+=p->GetPx();
	pysum+=p->GetPy();
	pzsum+=p->GetPz();
	esum+=p->GetE();
      }
      
 
    }
  }
  m=sqrt(esum*esum-pxsum*pxsum-pysum*pysum-pzsum*pzsum);

  if (option){
    if(strstr(option,"P")) {
      printf("---|--------|<-------;------->(---------------,---------------,----------------)---------------|---------------\n");
      printf("    S U M :                   (%15.9f,%15.9f,%15.9f) %15.9f %15.9f\n",pxsum,pysum,pzsum,esum,m);

    } else {
      
      printf("---|--------|-----|--------|--------|------|----------|---------|----------|---------|---------|--------|-------|--------|---------\n");
      
      printf("    S U M :                                 (%9.3f,%9.3f,%9.3f) %9.3f %9.3f\n",pxsum,pysum,pzsum,esum,m);
    }
  } else {

    printf("---|--------|-----|---|----|----------|---------|----------|---------|---------\n");
    printf("     S U M :                (%9.3f,%9.3f,%9.3f) %9.3f %9.3f\n",
	pxsum,pysum,pzsum,esum,m);


  }
}


int THEPEvent::GetIdOf(THEPParticle *particle)
{
  if (particle==0)
    return 0;
  int id=0;
  for (int i=1; i<=fNumOfParticles;i++) {
    THEPParticle* p_i= ((THEPParticle**)fParticles)[i];
    if (p_i->GetId() == particle->GetId()) {
      id=i;
      break;
    }
  }
  return id;
}


#ifdef _USE_ROOT_
//_____________________________________________________________________________
void THEPEvent::Streamer(TBuffer &R__b)
{
  // Stream an object of class THEPEvent.
  
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(); if (R__v) { }

      R__b >> fSize;
      R__b >> fNumOfParticles;
      R__b >> fEventNumber;
      
      if (fParticles) delete (fParticles);
      fParticles=(HEPParticle**) malloc(sizeof(THEPParticle*)*(fSize+1));
      for (int i=1;i<=fNumOfParticles;i++)
 	{
 	  THEPParticle *p=(THEPParticle*) malloc(sizeof(THEPParticle));
 	  //R__b >> p;
	  p->Streamer(R__b);
	  fParticles[i]=p;
	  p->SetEvent(this);
 	}
   } else {
      R__b.WriteVersion(THEPEvent::IsA());

      R__b << fSize;
      R__b << fNumOfParticles;
      R__b << fEventNumber;
       

      for (int i=1;i<=fNumOfParticles;i++)
 	{
 	  //R__b << ((THEPParticle**)fParticles)[i];
	  (((THEPParticle**)fParticles)[i])->Streamer(R__b);
	}


   }
}


////______________________________________________________________________________
//TBuffer &operator<<(TBuffer &buf, THEPEvent *&obj)
//{
// 
//         return buf;
//}        
////______________________________________________________________________________
//TBuffer &operator>>(TBuffer &buf, THEPEvent *&obj)
//{
// 
//         return buf;
//}        

#endif


