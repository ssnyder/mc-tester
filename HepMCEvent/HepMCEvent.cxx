/*
   HepMCEvent class implementation

   AUTHOR:      Nadia Davidson
   LAST UPDATE: 2008-02-07
*/
#include "HepMCEvent.H"
#include <iostream>
using namespace std;
using HepMC::GenEvent;

#ifdef _USE_ROOT_
ClassImp(HepMCEvent)
#endif

HepMCEvent::HepMCEvent( HepMC::GenEvent &e, bool include_self_decay){

  evt = &e;
  // Make a list of the particles in the event.
  // (Modifying these will not effect the GenParticles
  // in the HepMC::GenEvent). The pcle IDs given start at 1 
  // (and may differ from "barcode" in the GenEvent)
  count_self_decays=include_self_decay;
  
  m_particle_count = e.particles_size();
  
  particles = new HepMCParticle*[m_particle_count];
  GenEvent::particle_const_iterator pcle_itr = evt->particles_begin();
  GenEvent::particle_const_iterator pcle_itr_end = evt->particles_end();
  for(int i=0; pcle_itr != pcle_itr_end; pcle_itr++, i++){
    particles[i] = new HepMCParticle(**pcle_itr,this,i+1); 
  }
}
  
int HepMCEvent::GetNumOfParticles(){
  return m_particle_count;
}

void  HepMCEvent::SetNumOfParticles(int num){
  // Should throw some error as this can not be set
  cout << "Warning, should not be doing this for HepMCEvent" << endl;
}
  
int HepMCEvent::GetEventNumber(){
  return evt->event_number();
}

void HepMCEvent::SetEventNumber(int num){
  evt->set_event_number( num );
}


HEPParticle* HepMCEvent::GetParticle(int idx){
  if(idx < 1 || idx > GetNumOfParticles()){
    cout << "Warning can not get particle "<< idx;
    cout <<", particle ID not valid" << endl;
    return 0;
  }
  return particles[idx-1]; //Particle ID starts at 1
}

//Only implemented in HepMCEvent. Returns particle with GenEvent barcode.
HepMCParticle* HepMCEvent::GetParticleWithBarcode( int barcode ){
  for(int i=0; i <  GetNumOfParticles(); i++){
    if(particles[i]->part->barcode()==barcode)
      return particles[i];
  }
  cout << "Could not find particle with barcode "<<barcode<<endl;
  return 0; //and have some error about not finding the
            //particle
}

HEPParticleList* HepMCEvent::FindParticle(int pdg, HEPParticleList *list)
{
  // if list is not provided, it is created
  if (!list) list=new HEPParticleList();

  //loop over all particles in the event
  for (int i=1; i<=GetNumOfParticles(); i++) {
    HEPParticle * p = GetParticle(i);  
    if(p->GetPDGId()==pdg){
      list->push_back(p);
      HepMC::GenVertex * end = ((HepMCParticle *) p)->part->end_vertex();
      //if we want to ignore cases like tau->tau+gamma:
      if(!CountSelfDecays()&&end){
	//Check for daughters that are the same particle type
	HepMC::GenVertex::particles_out_const_iterator pcle_itr=end->particles_out_const_begin();
	HepMC::GenVertex::particles_out_const_iterator pcle_itr_end=end->particles_out_const_end();

	//If found, remove from list.
	for(; pcle_itr != pcle_itr_end; pcle_itr++ ){
	  if((*pcle_itr)->pdg_id() == pdg)
	    list->remove(p);
	}
      }
    }
  }
  return list;
}


//Methods not implemented
void  HepMCEvent::AddParticle( HEPParticle *p){}
void  HepMCEvent::SetParticle(int idx,HEPParticle *p){}
void  HepMCEvent::InsertParticle(int at_idx,HEPParticle *p){}
void  HepMCEvent::Clear(int fromIdx=1){}
void  HepMCEvent::AddParticle( int id, 
					 int pdgid,
					 int status,
					 int mother,
					 int mother2,
					 int firstdaughter,
					 int lastdaughter,
					 double E,
					 double px,
					 double py,
					 double pz,
					 double m,
					 double vx,
					 double vy,
					 double vz,
			       double tau){}

vector<double> * HepMCEvent::Sum4Momentum(){
  vector<double> * sum = new vector<double>(4,0.0);

  for(int i=0; i < GetNumOfParticles(); i++){
    if(particles[i]->IsStable()){
      sum->at(0)+=particles[i]->GetPx();
      sum->at(1)+=particles[i]->GetPy();
      sum->at(2)+=particles[i]->GetPz();
      sum->at(3)+=particles[i]->GetE();
    }
  }
  return sum;
}

HepMCEvent::~HepMCEvent(){ 
  for(int i=0; i < GetNumOfParticles(); i++){
    delete particles[i];
  }
  delete[] particles;
}
					 
#ifdef _USE_ROOT_
void HepMCEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}
#endif   
