/*
   Linkdef.h

   Used by ROOT to create dictionaries
*/
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class HepMCEvent-;
#pragma link C++ class HepMCParticle-;

#endif
