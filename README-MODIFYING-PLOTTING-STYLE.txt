To modify the way plots for booklets are created file
src/TDecayResults.cxx can be modified. It might be
particulary useful for creating plots for printing
purposes or plots without the ratio histogram.

All changes can be performed inside TDecayResult::Draw() function.
Few examples of such changes are:

1) Turning off SDP box
----------------------

Comment out lines:
  t1->Draw();
  t3->Draw();

near the end of the file.

2) Turn off histogram title
---------------------------

Add line (anywhere):
  hdiff->SetTitle(false);


3) Enlarge labels
-----------------

Add line (anywhere):
  hdiff->GetXaxis()->SetLabelSize(0.07);
  
For the other axis, after axis2 is created, add line:
  axis2->SetLabelSize(0.07);


4) Remove ratio histogram and ratio scale
-----------------------------------------

To remove histogram, add line (anywhere):
  hdiff->SetLineColor(10);

To remove axis, add lines (anywhere):
  hdiff->GetYaxis()->SetAxisColor(1);
  hdiff->GetYaxis()->SetLabelColor(10);
  hdiff->GetYaxis()->SetTickLength(0);

