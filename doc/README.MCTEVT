MCTEVT is MC-TESTER's own common block, that may be used to transfer "any"
event record data to MC-TESTER. Its syntax follows the syntax of HEPEVT
common block. The following code declares the use of MCTEVT event record
C----
      PARAMETER ( NMXMCT = 4000)
      REAL*8 PMCT,VMCT
      INTEGER NEVMCT,NMCT,ISTMCT,IDMCT,JMOMCT,JDAMCT
      REAL*4 P,V
      INTEGER N,K

      COMMON/MCTEVT/NEVMCT,NMCT,ISTMCT(NMXMCT),IDMCT(NMXMCT),
     *                JMOMCT(2,NMXMCT),JDAMCT(2,NMXMCT),
     *                PMCT(5,NMXMCT),VMCT(4,NMXMCT)
C----

It is required that the following data in the common block are filled:
    NMCT - number of particles in event record
    ISTMCT - status codes of particles, according to HEPEVT specification: 
		    1 means stable particle
		    2 means decaying particle
		    3 means "history entry"
    IDMCT - PDG codes for the particles
    JMOMCT - pointers to mothers
    JDAMCT - pointers to daughters
    PMCT - momentum/energy/mass of particles.

The remaining NEVMCT and VMCT variables are optional, not used by MC-TESTER.
They are kept for reasons of compatibility with HEPEVT event record.

The event stored in MCTEVT record must have a proper HEPEVT syntax, i.e.
all mother-daughter relationships in JMOMCT/JDAMCT should be filled properly!

At generation stage, to specify that the data should be taken from  MCTEVT 
common block, one should call MCSETUP(0,4) or put the following line
in SETUP.C file:

    Setup::EVENT=&MCTEVT;

For example of use of MCTEVT common block look at subroutine CNVMCT 
in examples-F77/pyhia/cnvmct-example directory. CNVMCT routine transfers
the data from LUJETS common block to MCTEVT common block.

-----------------------------------------------
Last modified: 26.10.2003 by Piotr Golonka