#include <stdio.h>
#include <assert.h>
#include "MC4Vector.H"
#include "HEPParticle.H"
#include "HEPEvent.H"

long int EnergyThresholdLimit(HEPParticle *mother,HEPParticleList *stableDaughters, int nparams, double *params)
{

    // PARAMETERS:
    // params[0] - threshold on p_t of particle expressed as a fraction of mother's energy 
    //			( p_t in mother's frame). If not specified - default is 0.05

    assert(mother!=0);
    assert(stableDaughters!=0);

    
    double threshold=0.05;
    
    if (nparams>0) {
	assert(params!=0);
	threshold=params[0];
    }
    
    double pt_limit=threshold * (mother->GetE());

    HEPParticleListIterator daughters (*stableDaughters);
    for (HEPParticle *part=daughters.first(); part!=0; part=daughters.next() ) {

	MC4Vector d4(part->GetE(),part->GetPx(),part->GetPy(),part->GetPz(),part->GetM());
	// boost to mother's frame:
	d4.Boost(mother->GetPx(),mother->GetPy(),mother->GetPz(),mother->GetE(),mother->GetM());

	double p_t=d4.Xt();
	
	if (p_t < pt_limit ) {
	    stableDaughters->remove(part);
	}
    }
    
    return 1;
};

