# 1 "tautest.F"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "tautest.F"
      PROGRAM TAUMAIN

      INTEGER NMXHEP
      PARAMETER (NMXHEP=4000)
      REAL*8  phep,  vhep ! to be real*4/ *8  depending on host
      INTEGER nevhep,nhep,isthep,idhep,jmohep,
     $        jdahep
      COMMON /hepevt/
     $      nevhep,               ! serial number
     $      nhep,                 ! number of particles
     $      isthep(nmxhep),   ! status code
     $      idhep(nmxhep),    ! particle ident KF
     $      jmohep(2,nmxhep), ! parent particles
     $      jdahep(2,nmxhep), ! childreen particles
     $      phep(5,nmxhep),   ! four-momentum, mass [GeV]
     $      vhep(4,nmxhep)    ! vertex [mm]


      INTEGER*8 I,MAXEVENT
      REAL*8 R
      
      MAXEVENT=100000

      

C Stage 1 (first generator)
      CALL MCSETUP(1,1)	




      
      CALL MCTHBK(123,100,0.D0,100.D0)
      
      CALL INIT_TAU
      CALL MCTEST(-1)
      
      DO I=1,MAXEVENT
      	    CALL GEN_TAU()
      	    CALL MCTEST(0)
            CALL MCTHFIL(123,1.D0*nhep,1.D0)
      
        IF ((10000*(I/10000)).EQ.I) THEN
           R=100.0*I/MAXEVENT
           WRITE(*,100),I,MAXEVENT,R
        ENDIF
      
      ENDDO
      	
      CALL MCTEST(20)
      CALL MCTEST(1)
      
 100  FORMAT('EVENT:',I12,'/',I12,'  (',F6.2,'%)')
      
      END




C---------------------------------------------

      SUBROUTINE INIT_TAU

C taken from taumain.f ...
      REAL POL(4)
      DOUBLE PRECISION HH(4)
C SWITCHES FOR TAUOLA;
      COMMON / JAKI   /  JAK1,JAK2,JAKP,JAKM,KTOM
      COMMON / IDFC  / IDFF
C I/O UNITS  NUMBERS
      COMMON / INOUT /  INUT,IOUT
C LUND TYPE IDENTIFIER FOR A1
      COMMON / IDPART / IA1
C /PTAU/ IS USED IN ROUTINE TRALO4
      COMMON /PTAU/ PTAU
      COMMON / TAURAD / XK0DEC,ITDKRC
      REAL*8            XK0DEC
      COMMON /TESTA1/ KEYA1


      KTORY=1
      KTO=2
      JAK1=0
      JAK2=0
      ITDKRC=0
      PTAU=0.
      XK0DEC=0.001


      JAK=0
C      JAK1=5
C      JAK2=5
C LUND IDENTIFIER (FOR TAU+) -15
      IF (KTORY.EQ.1) THEN
        IDFF=-15
      ELSE
        IDFF= 15
      ENDIF
C KTO=1 DENOTES TAU DEFINED BY IDFF (I.E. TAU+)
C KTO=2 DENOTES THE OPPOSITE        (I.E. TAU-)
      KTO=2

C TAU POLARIZATION IN ITS RESTFRAME;
      POL(1)=0.
      POL(2)=0.
      POL(3)=.9
        CALL INIMAS
        CALL INITDK


        CALL INIPHY(0.1D0)
      IF (KTORY.EQ.1) THEN
         CALL DEXAY(-1,POL)
      ELSE
         CALL DEKAY(-1,HH)
      ENDIF
      
      END


C----------------------------------

      SUBROUTINE GEN_TAU

      REAL POL(4)
      DOUBLE PRECISION HH(4)
      
      
      KTORY=1
      KTO=2
C TAU POLARIZATION IN ITS RESTFRAME;
      POL(1)=0.
      POL(2)=0.
      POL(3)=.9
      
      CALL TAUFIL
C DECAY....
      IF (KTORY.EQ.1) THEN
         CALL DEXAY(KTO,POL)
      ELSE
         CALL DEKAY(KTO,HH)
         CALL DEKAY(KTO+10,HH)
      ENDIF

      END

C-----------------------------
C  Dummy routines.
      SUBROUTINE LUHEPC
      END
      
      SUBROUTINE LULIST
      END
