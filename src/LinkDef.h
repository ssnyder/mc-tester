/*
   Linkdef.h

   Used by ROOT to create dictionaries
*/
#ifdef __CINT__

//#pragma link off all globals;
#pragma link off all classes;
//#pragma link off all functions;

#pragma link C++ class Setup;
#pragma link C++ class GenerationDescription;

#pragma link C++ class TDecayMode;
#pragma link C++ class TDecayResult;

#pragma link C++ class UserEventAnalysis;
#pragma link C++ class LC_EventAnalysis;
#pragma link C++ class HerwigEventAnalysis;
#pragma link C++ class MadGraphEventReader;

// pragmas for all F77 interfacing functions 
// are already in MCTester-F77.h


// pragmas for histogram analysis (MCTest01,...)
// are in respective include files.

#pragma link C++ function MC_Initialize();
#pragma link C++ function MC_Analyze(int);
#pragma link C++ function MC_Finalize();

#pragma link C++ function MC_FillUserHistogram(char*, double, double);

#pragma link C++ function KolmogorovAnalysis(TH1D*,TH1D*);
//#pragma link C++ function EnergyThresholdLimit(HEPParticle*,HEPParticleList*, int , double*);

#pragma link C++ global MCTEVT;

#endif
