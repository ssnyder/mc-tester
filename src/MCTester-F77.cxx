/*
   MC-TESTER FORTRAN interface implementation

   Parts of the code are for backward compatibility with the previous
   MC-TESTER versions
*/
#include "MCTester-F77.h"
#include "Generate.h"
#include "Setup.H"
#include "HEPEVTEvent.H"
#include "PYJETSEvent.H"
#include "LUJETSEvent.H"
#include "HerwigEvent.H"
#include "MCTesterEvent.H"
#include <math.h>
#include "TH1.h"
#include "TObjArray.h"
#include <cstdlib>

extern "C" void mctest_(int &mode)
{
    switch (mode) {
    
	case -1: MC_Initialize(); break;
	case  0: MC_Analyze(Setup::decay_particle); break;
	case  1: MC_Finalize(); break;
	case 20: Setup::EVENT->ls();break;
	case 21: PrintAnalysedEvent();break;
	default: printf("ERROR: mctest_() UNKNOWN mode %i\n",mode); exit(-1);
    }    

}

// version of MC-TESTER filling for weighted events...
extern "C" void mctestw_(int &mode, double &weight)
{
    if (mode==0) {
	MC_Analyze(Setup::decay_particle,weight); 
    } else {
	mctest_(mode);
    }

}



extern "C" void mcsetup_(int &what, int &value)
{
    switch(what) {
	case 0:
	    if      (value==0) Setup::EVENT=&HEPEVT;
	    else if (value==1) Setup::EVENT=&LUJETS;
	    else if (value==2) Setup::EVENT=&PYJETS;
	    else if (value==3) Setup::EVENT=&HerwigEVT;
	    else if (value==4) Setup::EVENT=&MCTEVT;
	    else {printf("ERROR in mcsetup_: Requested unknown event format:%i\n",value); exit(-1);}
	    break;
	case 1:
	    Setup::stage=value;		
	    break;
	case 2:
	    Setup::decay_particle=value;
	    break;
	default: printf("ERROR: mcsetup_() UNKNOWN request what=%i\n",what); exit(-1);    
    }


}



extern "C" void mcsetuphbins_(int &value)
{
 // set default number of bins
    for (int i=0; i<MAX_DECAY_MULTIPLICITY;i++)
    for (int j=0; j<MAX_DECAY_MULTIPLICITY;j++){
	Setup::nbins[i][j]=value;
	}
}

extern "C" void mcsetuphmin_(double &value)
{
// set default minimum bin
    for (int i=0; i<MAX_DECAY_MULTIPLICITY;i++)
    for (int j=0; j<MAX_DECAY_MULTIPLICITY;j++)
	Setup::bin_min[i][j]=value;
}

extern "C" void mcsetuphmax_(double &value)
{
// set default maximum bin
    for (int i=0; i<MAX_DECAY_MULTIPLICITY;i++)
    for (int j=0; j<MAX_DECAY_MULTIPLICITY;j++)
	Setup::bin_max[i][j]=value;
}


extern "C" void mcsetuphist_(int &nbody, int &nhist, int &nbins, double &minbin, double &maxbin)
{
// setup certain histogram

    if ( (nbody<0) || (nbody>=MAX_DECAY_MULTIPLICITY)) {
	fprintf(stderr,"ERROR in MCSETUPHIST() !\n");
	fprintf(stderr," specified nbody=%i is out of range[0,%i]\n",nbody,MAX_DECAY_MULTIPLICITY-1);
	exit(-1);
    }
    if ( (nhist<0) || (nhist>=MAX_DECAY_MULTIPLICITY)) {
	fprintf(stderr,"ERROR in MCSETUPHIST() !\n");
	fprintf(stderr," specified nhist=%i is out of range[0,%i]\n",nhist,MAX_DECAY_MULTIPLICITY-1);
	exit(-1);
    }

    Setup::nbins  [nbody][nhist]=nbins;
    Setup::bin_min[nbody][nhist]=minbin;
    Setup::bin_max[nbody][nhist]=maxbin;
}

/*****************************************
SUBROUTINE TOHEPI(N,KFB1,KFB2,
	 $ IDF1,IDF2,IDF3,IDF4,IDF5,IDF6,IDF7,IDF8,
	 $ XPB1,XPB2,AQF1,AQF2,AQF3,AQF4,AQF5,AQF6,AQF7,AQF8)
	
C     ===================================================
C     THIS IS THE ROUTINE YOU WILL USE:
C     N             - number of particles in final state
C     KFB1,KFB2     - beam identifiers
C     IDF1,IDF2,... - identifiers of FS particles
C     XPB1,XPB2,... - beam four momenta
C     AQF1,AQF2,... - final state partcls four momenta
C     IDFn,AQFn     - for n.GT.N are dummy
C     ===================================================

    DIMENSION  XPB1(4),XPB2(4),AQF1(4),AQF2(4)
    DIMENSION  AQF3(4),AQF4(4),AQF5(4),AQF6(4),AQF7(4),AQF8(4)
*******************************************************************************/

inline double MCT_InvMass(double v[4]) {
    return sqrt (v[3]*v[3]-v[0]*v[0]-v[1]*v[1] - v[2]*v[2]);
}


extern "C" void lctohep_(int &n,
    int &kfb1, int &kfb2,
    int &idf1, int &idf2, int &idf3, int &idf4, 
    int &idf5, int &idf6, int &idf7, int &idf8,
    double xpb1[4], double xpb2[4], 
    double aqf1[4], double aqf2[4], double aqf3[4], double aqf4[4], 
    double aqf5[4], double aqf6[4], double aqf7[4], double aqf8[4])
{



if (n>8) {
    printf("ERROR: LCTOHEP() cannot fill more than 2->8: you have specified n=%i\n",n);
    exit(-1);
}
HEPEVT.Clear(1);
	 HEPEVT.AddParticle(1, kfb1, 3, 0, 0, 3, n, xpb1[3], xpb1[0], xpb1[1], xpb1[2], MCT_InvMass(xpb1) ,0.0, 0.0, 0.0, 0.0);
         HEPEVT.AddParticle(2, kfb2, 3, 0, 0, 3, n, xpb2[3], xpb2[0], xpb2[1], xpb2[2], MCT_InvMass(xpb2) ,0.0, 0.0, 0.0, 0.0);
if (n>0) HEPEVT.AddParticle(3, idf1, 1, 1, 2, 0, 0, aqf1[3], aqf1[0], aqf1[1], aqf1[2], MCT_InvMass(aqf1) ,0.0, 0.0, 0.0, 0.0);
if (n>1) HEPEVT.AddParticle(4, idf2, 1, 1, 2, 0, 0, aqf2[3], aqf2[0], aqf2[1], aqf2[2], MCT_InvMass(aqf2) ,0.0, 0.0, 0.0, 0.0);
if (n>2) HEPEVT.AddParticle(5, idf3, 1, 1, 2, 0, 0, aqf3[3], aqf3[0], aqf3[1], aqf3[2], MCT_InvMass(aqf3) ,0.0, 0.0, 0.0, 0.0);
if (n>3) HEPEVT.AddParticle(6, idf4, 1, 1, 2, 0, 0, aqf4[3], aqf4[0], aqf4[1], aqf4[2], MCT_InvMass(aqf4) ,0.0, 0.0, 0.0, 0.0);
if (n>4) HEPEVT.AddParticle(7, idf5, 1, 1, 2, 0, 0, aqf5[3], aqf5[0], aqf5[1], aqf5[2], MCT_InvMass(aqf5) ,0.0, 0.0, 0.0, 0.0);
if (n>5) HEPEVT.AddParticle(8, idf6, 1, 1, 2, 0, 0, aqf6[3], aqf6[0], aqf6[1], aqf6[2], MCT_InvMass(aqf6) ,0.0, 0.0, 0.0, 0.0);
if (n>6) HEPEVT.AddParticle(9, idf7, 1, 1, 2, 0, 0, aqf7[3], aqf7[0], aqf7[1], aqf7[2], MCT_InvMass(aqf7) ,0.0, 0.0, 0.0, 0.0);
if (n>7) HEPEVT.AddParticle(10, idf8, 1, 1, 2, 0, 0, aqf8[3], aqf8[0], aqf8[1], aqf8[2], MCT_InvMass(aqf8) ,0.0, 0.0, 0.0, 0.0);
																				
}


extern "C" void mcthbk_( int &hist_id, int &nbins, double &bin_min, double &bin_max)
{
/*
        SUBROUTINE MCTHBK(HIST_ID, NBINS, BIN_MIN, BIN_MAX)
        
	INTEGER HIST_ID
	INTEGER NBINS
	REAL*8 BIN_MIN
	REAL*8 BIN_MAX
 
 C---------------------------------------------------------------------
 C Creates user's histogram in the MC-TESTER infrastructure.
 C Histogram may then be filled using CALL MCTHFIL(HIST_ID,VALUE,W)
 C
 C HIST_ID - unique integer ID. In the root file histogram will
 C           be called h_USER_XXXX wher XXXX is specified HIST_ID
 C
 C NBINS   - number of bins in histogram
 C BIN_MIN - lower edge of histogram
 C BIN_MAX - upper edge of histogram
 C---------------------------------------------------------------------
*/
    char histname[128];
    char histtitle[128];
    sprintf(histname,"h_USER_%04i",hist_id);
    sprintf(histtitle,"user histogram %i",hist_id);

    printf("creating histogram %s, bins=%i [%f,%f]\n",histname,nbins,bin_min,bin_max);
    TH1D *h=new TH1D(histname,histtitle,nbins,bin_min,bin_max);
    h->Sumw2();
    Setup::user_histograms->AddLast(h);

}

extern "C" void mcthfil_( int &hist_id, double &value, double &weight)
{
/*
        SUBROUTINE MCTHFIL(HIST_ID, VALUE, WEIGHT)
        
	INTEGER HIST_ID
	REAL*8 VALUE
	REAL*8 WEIGHT
*/
    char hname[256];
    sprintf(hname,"h_USER_%04i",hist_id);

    TH1D *h=(TH1D*)(Setup::user_histograms->FindObject(hname));
    if (!h) {
	printf("ERROR in SUBROUTINE MCTHFIL() : histogram %i not defined!\n",hist_id);
	exit(-1);
    }

    h->Fill(value,weight);
}

